# 1.10 确保 SSH 远程访问权限受到管控

## 安全等级

- Level 2

## 描述

通过对 SSH 远程访问的限制，可确保只有已授权的用户才可访问系统，对系统进行操作和管理。

对 SSH 远程访问的限制，可通过更新`/etc/ssh/sshd_config`配置文件，添加以下参数来实现：

* AllowUsers：
  * `AllowUsers`参数的作用是：允许特定用户通过`SSH`进行远程访问。该列表由空格分隔的用户名组成（不能识别UID）。可使用`user@host`的形式：仅允许某用户通过特定的主机登录，对权限进行更精细的限制。

* AllowGroups：
  * `AllowGroups`参数的作用是：允许特定用户组通过`SSH`进行远程访问。该列表由空格分隔的组名组成（不能识别GID）。

* DenyUsers：
  * `DenyUsers`参数的作用是：禁止特定用户通过`SSH`进行远程访问。该列表由空格分隔的用户名组成（不能识别UID）。可使用`user@host`的形式：仅禁止某用户通过特定的主机登录，对权限进行更精细的限制。

* DenyGroups：
  * `DenyGroups`参数的作用是：禁止特定用户组通过`SSH`进行远程访问。该列表由空格分隔的组名组成（不能识别GID）。

至少利用以上一个参数，对 SSH 远程访问进行限制。

## 修复建议

目标：更新`/etc/ssh/sshd_config`配置文件，对 SSH 远程访问权限进行限制。

1. 更新`/etc/ssh/sshd_config`配置文件，至少添加以下4个权限控制参数中的1个或更多：

```bash
AllowUsers <userlist>
AllowGroups <grouplist>
DenyUsers <userlist>
DenyGroups <grouplist>
```

`<userlist>`、`<grouplist>`、`<userlist>`、`<grouplist>`为使用空格分隔的自定义用户名列表或组列表。

## 扫描检测

确保`/etc/ssh/sshd_config`的权限配置正确

1. 执行以下命令，并查看输出结果：

```bash
# sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -Pi '^\h*(allow|deny)(users|groups)\h+\H+(\h+.*)?$'
# grep -Pi '^\h*(allow|deny)(users|groups)\h+\H+(\h+.*)?$' /etc/ssh/sshd_config
```

2.执行第`1`步的两条命令的输出结果至少匹配以下4条结果中的一条：

```bash
allowusers <userlist>
allowgroups <grouplist>
denyusers <userlist>
denygroups <grouplist>
```

`<userlist>`、`<grouplist>`、`<userlist>`、`<grouplist>`为使用空格分隔的自定义用户名列表或组列表。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>