# 1.18 确保 SSH PermitEmptyPasswords 参数正确配置

## 安全等级

- Level 1

## 描述

SSH 配置文件：`/etc/ssh/sshd_config`中的`PermitEmptyPasswords`参数指定用户是否可以使用空字符串密码进行 SSH 登录。

禁止密码为空的帐户进行远程 SSH 登录，可有效减少未授权用户登录的可能性。

## 修复建议

对`/etc/ssh/sshd_config`配置文件的`PermitEmptyPasswords`参数进行配置。

1. 编辑`/etc/ssh/sshd_config`配置文件，修改`PermitEmptyPasswords`参数，或添加以下代码，对`PermitEmptyPasswords`参数进行配置：

```bash
PermitEmptyPasswords no
```

## 扫描检测

确保 SSH PermitEmptyPasswords 参数正确配置。

1. 执行以下命令，验证`SSH`的`PermitEmptyPasswords`配置是否正确：

```bash
# sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep permitemptypasswords
permitemptypasswords no
# grep -Ei '^\s*PermitEmptyPasswords\s+yes' /etc/ssh/sshd_config
Nothing is returned
```

如果第一条命令执行后返回`no`，且第二条命令执行后，没有返回任何结果，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>