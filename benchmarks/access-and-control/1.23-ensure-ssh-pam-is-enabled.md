# 1.23 确保 SSH PAM 已启用

## 安全等级

- Level 1

## 描述

PAM（Pluggable Authentication Modules）是由 Sun 提出的一种认证机制。

它通过提供一些动态链接库和一套统一的API，将系统提供的认证服务与该服务的认证方式分开，使得系统管理员可以灵活的根据需要给不同的服务配置不同的认证方式而无需更改服务程序，同时也便于向系统中添加新的认证手段。
`UsePAM` 参数如果设置为 "yes"，将启用PAM认证。

注意：如果启用`UsePAM`，将不能以非`root`用户的身份运行`sshd(8)`。

## 修复建议

对`/etc/ssh/sshd_config`配置文件的`UsePAM`参数进行配置。

1. 编辑`/etc/ssh/sshd_config`配置文件，修改`UsePAM`参数，或添加以下代码，对`UsePAM`参数进行配置：

```bash
UsePAM yes
```

默认值为`yes`。

## 扫描检测

确保 SSH PAM 已启用。

1. 执行以下命令，验证`SSH`的`UsePAM`配置是否正确：

```bash
# sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -i usepam
usepam yes
# grep -Ei '^\s*UsePAM\s+no' /etc/ssh/sshd_config
Nothing is returned
```

如果第一条命令执行后返回`yes`，且第二条命令执行后，没有返回任何结果，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>