# 1.34 确保不活跃用户的锁定时间为 30 天或更短

## 安全等级

- Level 1

## 描述

长时间不活跃（密码过期）的用户，应自动将其禁用。建议设置为：禁用在密码过期30天后仍未重新激活的用户。

## 修复建议

修改密码未激活锁定时间。

1. 执行以下命令，将密码未激活锁定时间设置为30天。

```bash
# useradd -D -f 30
```

默认值为`-1`，即不锁定，需修改为建议值。

2. 将密码未激活锁定时间策略应用于所有用户：

```bash
# getent passwd | cut -f1 -d ":" | xargs -n1 chage --inactive 30
```

## 扫描检测

确保密码未激活锁定时间是30天或更短。

1. 执行以下命令，密码未激活锁定时间是否符合要求：

```bash
# useradd -D | grep INACTIVE
INACTIVE=30
```

2. 检查用户列表，确认所有用户的密码未激活锁定时间符合要求：

```bash
# grep -E ^[^:]+:[^\!*] /etc/shadow | cut -d: -f1,7
<user>:<INACTIVE>
```

* 其中`<user>`为用户名，`<INACTIVE>`为密码未激活锁定天数，如：`root:30`。

如配置文件及所有用户的密码未激活锁定天数均`<=30`，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>