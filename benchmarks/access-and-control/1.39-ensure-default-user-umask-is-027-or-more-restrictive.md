# 1.39 确保 umask 设置为 027 或更严格

## 安全等级

- Level 1

## 描述

umask 是用来指定新创建目录和文件的默认文件权限的。在Linux中，所有新创建的目录的默认权限是 0777（rwxrwxrwx），而所有新创建的文件的默认权限是 0666（rw-rw-rw-）。umask 通过限制（屏蔽）这些权限来修改Linux的默认权限。umask 不是简单的减法，而是按位处理。在生成新文件时，其默认权限会减去 umask 中设置的位。

umask 可以用八进制数字（4、2、1）或字母（r、w、x）来设置：

* 八进制数字   ->   由三位或四位数字表示，即`umask 0027`或`umask 027`。 如果使用四位数的掩码，第一位数会被忽略。剩下的三位数字分别影响 user、group、other 的权限。
* 字母   ->   用逗号分隔的列表表示 user、group、other 的权限。
* 例：`umask u=rwx,g=rx,o=`即等于八进制数字的`umask 027`，此设置会将新建目录的默认权限配置为`drwxr-x---`，将新建文件的默认权限设置为`rw-r-----`。

文件和目录的真正初始权限，可通过以下的计算得到：

```
文件（或目录）的初始权限 = 文件（或目录）的最大默认权限 - umask权限
```

配置 umask 值的方法：

* pam_umask 模块：
  * 在`/etc/login.defs`中添加或修改`umask`参数（使用八进制数字格式）。
* 系统 Shell 配置文件（作用于所有用户）：
  * `/etc/profile`   ->   用于为用户的 shell 设置系统范围的环境变量。
  * `/etc/profile.d`   ->   会在系统启动或登录 shell 时，执行该目录下所有以`*.sh`结尾的文件。
  * `/etc/bashrc`   ->   为每一个运行bash shell的用户执行此文件，当bash shell被打开时,该文件被读取。
* 用户 shell 配置文件（作用于当前用户）：
  * `~/.bash_profile`   ->   用户在登录时，会读取此文件中的配置。
  * `~/.bashrc`   ->   该文件存储的是专属于个人bash shell的信息，当登录时以及每次打开一个新的shell时,执行这个文件，在这个文件里可以自定义用户专属的个人信息。

为 umask 设置一个安全的默认值可以避免新建的目录或文件具有过多的权限，被未经授权的用户读取或篡改。

## 修复建议

检查`/etc/bashrc`、`/etc/profile`以及`/etc/profile.d/`目录中所有以`*.sh`结尾的文件，添加或编辑所有的`umask`条目，使其符合安全要求：`umask 027`，`umask u=rwx,g=rx,o=`或更加严格的限制。

1. 在以下文件中配置`umask`参数
* `/etc/bashrc`
* `/etc/profile`
* `/etc/profile.d/`目录中所有以`*.sh`结尾的文件：

可使用以下命令，检查`/etc/bashrc`、`/etc/profile`以及`/etc/profile.d/`目录中所有已配置的`umask`参数：

```bash
# grep -RPi '(^|^[^#]*)\s*umask\s+([0-7][0-7][01][0-7]\b|[0-7][0-7][0-7][0-6]\b|[0-7][01][0-7]\b|[0-7][0-7][0-6]\b|(u=[rwx]{0,3},)?(g=[rwx]{0,3},)?o=[rwx]+\b|(u=[rwx]{1,3},)?g=[^rx]{1,3}(,o=[rwx]{0,3})?\b)' /etc/login.defs /etc/profile* /etc/bashrc*
/etc/login.defs:UMASK		022
/etc/profile:    umask 002
/etc/bashrc:       umask 002
```

根据以上结果，对`umask`参数进行修改，例如：

```bash
# vi /etc/profile
umask 027
# vi /etc/profile.d/set_umask.sh
umask 027
```

2. 添加或修改`/etc/login.defs`文件中的`UMASK`及`USERGROUPS_ENAB`参数。

```bash
UMASK 027
USERGROUPS_ENAB no
```

3. 编辑`/etc/pam.d/password-auth`及`/etc/pam.d/system-auth`，添加或编辑以下内容:

```bash
session optional pam_umask.so
```

默认的 UMASK 配置为：`UMASK 022`。

## 扫描检测

确保 umask 设置为 027 或更严格。

1. 执行以下命令，验证 umask 参数是否符合以下要求：

* 创建目录的默认权限为750 (drwxr-x---)，创建文件的默认权限为640 (rw-r-----)，或者更严格。

执行以下命令，观察返回结果：

```bash
#!/bin/bash
passing=""
grep -Eiq '^\s*UMASK\s+(0[0-7][2-7]7|[0-7][2-7]7)\b' /etc/login.defs && grep -Eqi '^\s*USERGROUPS_ENAB\s*"?no"?\b' /etc/login.defs && grep -Eq '^\s*session\s+(optional|requisite|required)\s+pam_umask\.so\b' /etc/pam.d/common-session && passing=true
grep -REiq '^\s*UMASK\s+\s*(0[0-7][2-7]7|[0-7][2-7]7|u=(r?|w?|x?)(r?|w?|x?)(r?|w?|x?),g=(r?x?|x?r?),o=)\b' /etc/profile* /etc/bashrc* && passing=true
[ "$passing" = true ] && echo "Default user umask is set"
```

如返回`Default user umask is set`，则视为通过。

```bash
# grep -RPi '(^|^[^#]*)\s*umask\s+([0-7][0-7][01][0-7]\b|[0-7][0-7][0-7][0-6]\b|[0-7][01][0-7]\b|[0-7][0-7][0-6]\b|(u=[rwx]{0,3},)?(g=[rwx]{0,3},)?o=[rwx]+\b|(u=[rwx]{1,3},)?g=[^rx]{1,3}(,o=[rwx]{0,3})?\b)' /etc/login.defs /etc/profile* /etc/bashrc*
No file should be returned
```

如没有返回任何结果，则视为通过。

如以上两组命令的输出结果，均符合要求，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>