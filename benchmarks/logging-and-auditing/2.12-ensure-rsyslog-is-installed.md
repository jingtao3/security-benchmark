# 2.12 确保已安装 rsyslog

## 安全等级

- Level 1

## 描述

rsyslog 软件是原 syslogd 守护程序的替代品，其相比于 syslogd 做了很多改进，添加了如：面向连接（即TCP）的日志传输、将日志记录到数据库，以及在与中央日志服务器交互中对日志数据进行加密等特性及功能。

以上多种新特性与功能，都证明了安装和配置 rsyslog 软件包的合理性及必要性。

## 修复建议

目标：安装`rsyslog`软件包。

1. 运行以下命令安装`rsyslog`：

```bash
# yum install rsyslog -y
```

## 扫描检测

确保已安装 rsyslog。

1. 执行以下命令，检查 rsyslog 软件是否正确安装：

```bash
# rpm -q rsyslog
rsyslog-<version>
```

`<version>`为版本号，如：`rsyslog-8.2102.0-5.an8.x86_64`

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>
