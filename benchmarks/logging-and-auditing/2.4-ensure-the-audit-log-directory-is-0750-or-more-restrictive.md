# 2.4 确保审计目录的权限小于 0750

## 安全等级

- Level 1

## 描述

审计信息（审计记录、审计设置、审计报告）含有系统配置、用户数据、登录信息、操作记录等敏感数据。需正确配置其所在目录的权限，避免非授权用户篡改或删除审计数据，破坏审计信息的准确性，影响系统管理人员对恶意活动的发现和判断。

通过对**审计目录**的权限进行配置，确保其不被非授权用户篡改或删除。

## 修复建议

目标：确保审计目录的权限小于`0750`

1. 执行以下命令来确定审计目录的路径：

```bash
# grep -iw ^log_file /etc/audit/auditd.conf
log_file = /var/log/audit/audit.log
```

2. 根据上一步得到的审计目录路径，执行以下命令，将审计日志目录的权限配置为`0750`或更小：

```bash
# chmod -R g-w,o-rwx /var/log/audit
```

## 扫描检测

确保审计目录的权限小于`0750`

1. 执行以下命令来确定审计目录的路径：

```bash
# grep -iw ^log_file /etc/audit/auditd.conf
log_file = /var/log/audit/audit.log
```

2. 根据上一步得到的审计目录路径，执行以下命令，检查审计目录的权限是否为`0750`或更小：

```bash
# stat -c "%n %a" /var/log/audit /var/log/audit/*
/var/log/audit 750 
```

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>