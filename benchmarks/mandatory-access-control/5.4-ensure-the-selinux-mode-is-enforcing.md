# 5.4 确保SELinux是Enforcing模式

## 安全等级

- Level 3

## 描述

避免系统强制执行 SELinux 策略，还避免标记任何持久性对象，例如文件，从而使未来难以启用 SELinux。
SELinux提供了三种模式，分别是：
    Enforcing：强制模式。违反SELinux规则的行为将被阻止并记录到日志中。 
    Permissive：宽容模式。违反SELinux 规则的行为只会记录到日志中。一般为调试用。 
    Disabled：关闭SELinux。

## 修复建议

目标：确保SELinux是Enforcing模式。

1. 运行以下命令设置成Enforcing模式:
```bash
# setenforce 1
```
2. 也可以修改 /etc/selinux/config 文件进行设置成Enforcing模式：
```bash
SELINUX=enforcing
```

## 扫描检测

1. 命令查看当前SELinux的模式：
```bash
# getenforce
Enforcing
```

2. 运行以下命令查看当前SELinux的模式：
```bash
# grep -Ei '^\s*SELINUX=(enforcing|permissive)' /etc/selinux/config
SELINUX=enforcing
```

如输出结果为enforcing那么符合预期，则视为通过此项检查

## 参考

SELinux States and Modes:https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/selinux_users_and_administrators_guide/sect-security-enhanced_linux-introduction-selinux_modes
