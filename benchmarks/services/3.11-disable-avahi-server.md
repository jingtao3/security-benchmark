# 3.11 禁用Avahi Server

## 安全等级

- Level 1

## 描述

Avahi 是一个免费的 zeroconf（零配置网络服务规范） 实现，包括用于多播 DNS/DNS-SD 服务发现的系统。 Avahi 允许程序在没有特定配置的情况下发布和发现在本地网络上运行的服务和主机。 例如，用户可以将计算机插入网络，Avahi 会自动查找要打印到的打印机、要查看的文件和要交谈的人，以及机器上运行的网络服务。

系统功能通常不需要自动发现网络服务。 建议禁用该服务以减少潜在的攻击面。

## 修复建议

运行以下命令来禁用`avahi-daemon.socket`和`avahi-daemon.service`

```bash
# systemctl --now disable avahi-daemon.socket
# systemctl --now disable avahi-daemon.service
```

## 扫描检测

1. 运行以下命令来检查`avahi-daemon.socket`是否被禁用

```bash
# systemctl is-enabled avahi-daemon.socket
```

期待的输出结果**不是**`enabled`。

2. 运行以下命令来检查`avahi-daemon.serivce`是否被禁用

```bash
# systemctl is-enabled avahi-daemon
```

期待的输出结果**不是**`enabled`。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>
