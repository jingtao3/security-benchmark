# 3.15 禁用IMAP 和POP3 Server

## 安全等级

- Level 1

## 描述

dovecot 是基于 Linux 系统的开源 IMAP 和 POP3 服务器。

除非该系统要提供 POP3 或 IMAP 服务器，否则建议禁用该服务以减少潜在的攻击面。

## 修复建议

运行以下命令来禁用`dovecot`

```bash
# systemctl --now disable dovecot
```

## 扫描检测

运行以下命令来检查`dovecot`是否被禁用

```bash
# systemctl is-enabled dovecot
```

期待的输出结果**不是**`enabled`

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>
