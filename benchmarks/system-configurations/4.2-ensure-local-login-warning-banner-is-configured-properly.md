# 4.2 确保本地登录提示消息的内容符合要求

## 安全等级

- Level 1

## 描述

在**本地终端**登录时会展示`/etc/issue`文件中的内容。

类 Unix 系统通常会在登录系统时显示当前操作系统的版本信息和补丁信息。这些信息对于为特定操作系统平台开发软件的开发者来说是很有用的。但这样做也有一个副作用：会将系统版本的详细提供给针对特定系统漏洞的攻击者。而这些信息，对于已授权的用户来说，可以通过`uname -a`命令轻松获得。

综上，登录提示信息不应展示操作系统版本信息及补丁信息。

`/etc/issue`文件的内容解析：

```bash
# cat /etc/issue
\S
Kernel \r on an \m
```

其中`\*`参数对应输出内容如下：

```
\m 给出当前操作系统的位数
\r 详细的内核版本
\s 操作系统的名称
\v 操作系统的版本
```

## 修复建议

编辑或删除`/etc/issue`文件，确保系统信息不展示在登录消息内。

1. 可根据使用环境，自行编辑登录提示消息，但要确保其内容没有系统版本或补丁信息。删除任何含有`\m`、`\r`、`\s`、`\v`的信息。

* 参考内容：

```bash
# echo "Authorized uses only. All activity may be monitored and reported." > /etc/issue
```

## 扫描检测

确保登录提示消息的内容符合要求。

1. 执行以下命令，检查输出内容是否有不符合安全要求的信息（系统版本信息、补丁信息等）：

```bash
# cat /etc/issue
Authorized uses only. All activity may be monitored and reported.
```

2. 执行以下命令，确保没有返回任何信息：

```bash
# grep -E -i "(\\\v|\\\r|\\\m|\\\s|$(grep '^ID=' /etc/os-release | cut -d= -f2 | sed -e 's/"//g'))" /etc/issue
No information is returned.
```

如`/etc/issue`内没有任何不符合安全要求的信息（系统版本信息、补丁信息等），且第2条命令执行后没有返回任何内容，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>