# 4.31 确保用户的主目录权限为 750 或更严格

## 安全等级

- Level 1

## 描述

在创建新用户时，系统管理员会为用户的主目录配置符合安全要求的权限，但后期用户可以很容易地修改这些权限。

如果用户主目录的权限对 group 与 other 可写，可能使恶意用户窃取或修改其他用户的数据，或获得其他用户的系统特权。

建议对所有用户的主目录权限进行限制，配置为 750 或更严格权限策略。

## 修复建议

配置用户主目录的权限。

* 在不通知用户的情况下对用户主目录进行全局修改可能会导致程序意外中断和用户不满。因此建议建立监控策略，及时上报用户文件权限，并根据实际情况，判断采取的措施。

1. 执行以下脚本，删除用户主目录 750 以上的权限：

```bash
#!/bin/bash
awk -F: '($1!~/(halt|sync|shutdown|nfsnobody)/ &&
$7!~/^(\/usr)?\/sbin\/nologin(\/)?$/ && $7!~/(\/usr)?\/bin\/false(\/)?$/) {print $6}' /etc/passwd | while read -r dir; do
 if [ -d "$dir" ]; then
    dirperm=$(stat -L -c "%A" "$dir")
    if [ "$(echo "$dirperm" | cut -c6)" != "-" ] || [ "$(echo "$dirperm" | cut -c8)" != "-" ] || [ "$(echo "$dirperm" | cut -c9)" != "-" ] || [ "$(echo "$dirperm" | cut -c10)" != "-" ]; then
    	chmod g-w,o-rwx "$dir"
    fi
 fi
done
```

## 扫描检测

确保用户的主目录权限为 750 或更严格。

1. 执行以下脚本，检查返回结果：

```bash
#!/bin/bash
awk -F: '($1!~/(halt|sync|shutdown|nfsnobody)/ && 
$7!~/^(\/usr)?\/sbin\/nologin(\/)?$/ && $7!~/(\/usr)?\/bin\/false(\/)?$/) {print $1 " " $6}' /etc/passwd | while read -r user dir; do
 if [ ! -d "$dir" ]; then
 echo "User: \"$user\" home directory: \"$dir\" doesn't exist"
 else
	dirperm=$(stat -L -c "%A" "$dir")
	if [ "$(echo "$dirperm" | cut -c6)" != "-" ] || [ "$(echo "$dirperm" | cut -c8)" != "-" ] || [ "$(echo "$dirperm" | cut -c9)" != "-" ] || [ "$(echo "$dirperm" | cut -c10)" != "-" ]; then
 echo "User: \"$user\" home directory: \"$dir\" has permissions: \"$(stat -L -c "%a" "$dir")\""
	fi
 fi
done
```

如没有任何返回值，则视为通过此项检查。


## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>