# 4.5 确保 /etc/issue 的权限配置正确

## 安全等级

- Level 1

## 描述

在本地终端登录时会展示`/etc/issue`文件中的内容。

如果`/etc/issue`文件的权限及所有者没有正确配置，其内容就可能被未经授权的用户篡改，从而展示不正确或误导性的登录信息。

## 修复建议

目标：正确配置`/etc/issue`文件的权限和所有者。

1. 使用以下代码，配置`/etc/issue`文件的权限和所有者：

```bash
# chown root:root /etc/issue
# chmod u-x,go-wx /etc/issue
```

## 扫描检测

确保`/etc/issue`目录的权限配置正确。

1. 执行以下命令，检查`/etc/issue`目录的权限属性：

```bash
# stat /etc/issue
Access: (0644/-rw-r--r--)  Uid: (    0/    root)   Gid: (    0/    root)
```

如果输出结果中：`Uid`与`Gid`均为`0/root`，且`Acces`为`0644`或更加严格，则视为通过此项检查。

## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>