# 4.8 确保正确安装 AIDE

## 安全等级

- Level 1

## 描述

AIDE (Advanced Intrusion Detection Environment)是一种入侵检测工具，它在Linux操作系统下使用预定义的规则对文件和目录的完整性进行检测。

AIDE有自己的数据库来检查文件和目录的完整性：AIDE获取文件和目录的快照，包括修改时间、权限和文件哈希值，然后使用该快照与文件系统的当前状态进行比较，以检测文件系统的修改。

## 修复建议

目标：正确安装`AIDE`。

1. 执行以下命令，安装`AIDE`软件：

```bash
# dnf install aide -y
```

2. 初始化`AIDE`服务：

```bash
# aide --init
# mv /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz
```

## 扫描检测

确保正确安装 AIDE。

1. 执行以下命令，验证是否正确安装了AIDE：

```bash
# rpm -q aide
aide-<version>
```

`<version>`为版本号，如：`aide-0.16-14.an8_5.1.x86_64`。如有返回`aide`及版本号，则视为通过此项检查。


## 参考

- cis: <https://www.cisecurity.org/benchmark/aliyun_linux>