if [ "$(rpm -qa httpd)" ]; then
    result=$(systemctl is-enabled httpd)
    if [ $result != enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi