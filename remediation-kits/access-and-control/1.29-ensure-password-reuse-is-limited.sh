#!/bin/bash
if authselect current | awk 'NR == 1 {print $3}' | grep -q custom/; then
    PTF=/etc/pam.d/"$(authselect current | awk 'NR == 1 {print $3}' | grep custom/)"/system-auth
else
    PTF=/etc/pam.d/system-auth
fi
if grep -Eq '^\s*password\s+(sufficient\s+pam_unix|requi(red|site)\s+pam_pwhistory).so\s+([^#]+\s+)*remember=\S+\s*.*$' $PTF; then
    sed -ri 's/^\s*(password\s+(requisite|sufficient)\s+(pam_pwhistory\.so|pam_unix\.so)\s+)(.*)(remember=\S+\s*)(.*)$/\1\4 remember=5 \6/' $PTF
else
    sed -ri 's/^\s*(password\s+(requisite|sufficient)\s+(pam_pwhistory\.so|pam_unix\.so)\s+)(.*)$/\1\4 remember=5/' $PTF
fi