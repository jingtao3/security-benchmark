#!/bin/bash

grep -qiP '^Protocol' /etc/ssh/sshd_config && sed -i "/^Protocol/cProtocol 2" /etc/ssh/sshd_config || echo -e "Protocol 2" >> /etc/ssh/sshd_config
systemctl restart sshd
