passMaxDaysRowNum=($(cat /etc/login.defs | awk '{if($1 == "PASS_MAX_DAYS") print NR}'))
targetString="PASS_MAX_DAYS	90"
if [ -n "$passMaxDaysRowNum" ]; then
	sed -i "${passMaxDaysRowNum} c ${targetString}" /etc/login.defs;
fi
getent passwd | cut -f1 -d ":" | xargs -n1 chage --maxdays 90