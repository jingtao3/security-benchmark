passMinDaysRowNum=($(cat /etc/login.defs | awk '{if($1 == "PASS_MIN_DAYS") print NR}'))
targetString="PASS_MIN_DAYS	7"
if [ -n "$passMinDaysRowNum" ]; then
	sed -i "${passMinDaysRowNum} c ${targetString}" /etc/login.defs;
fi
getent passwd | cut -f1 -d ":" | xargs -n1 chage --mindays 7