passSuffRowNum=($(cat /etc/pam.d/system-auth | awk '{if($1 == "password" && $2 == "sufficient")print NR}'))
targetString="password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok remember=5"
if [ -n "$passSuffRowNum" ]; then
	sed -i "${passSuffRowNum} c ${targetString}" /etc/pam.d/system-auth;
fi