#!/usr/bin/env bash
grep -qs "\-a always,exit -F path=/usr/bin/chsh -F perm=x -F auid>=1000 -F auid!=4294967295 -k priv_cmd" /etc/audit/rules.d/stig.rules || echo -e "-a always,exit -F path=/usr/bin/chsh -F perm=x -F auid>=1000 -F auid!=4294967295 -k priv_cmd" >> /etc/audit/rules.d/stig.rules
augenrules --load