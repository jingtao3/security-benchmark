chown root:root /etc/motd
chmod u-x,go-wx /etc/motd
[[ -f /var/lib/update-motd/motd ]] && chown root:root /var/lib/update-motd/motd
[[ -f /var/lib/update-motd/motd ]] && chmod u-x,go-wx /var/lib/update-motd/motd