result_root=false
result_ssh_keys=false

find /etc/ssh -xdev -type f -name 'ssh_host_*_key' -exec stat -c %G-%U-%a {} \; | grep -Piq "root\-root\-([7][1-7][1-7]|[0-7][1-7][1-7])" || result_root=true
find /etc/ssh -xdev -type f -name 'ssh_host_*_key' -exec stat -c %G-%U-%a {} \; | grep -Piq "ssh_keys\-root\-([7][5-7][1-7]|[0-7][5-7][1-7])" || result_ssh_keys=true

if [[ "$result_root" == true && "$result_ssh_keys" == true ]]; then
    echo "pass"
else
    echo "fail"
fi
