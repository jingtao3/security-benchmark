result_logingracetime=0
result_LoginGraceTime_sshd_config=false

val_logingracetime=`sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep logingracetime | cut -d ' ' -f 2`
result_logingracetime=`echo "$val_logingracetime <= 60" | bc`

grep -Ei '^\s*LoginGraceTime\s+(0|6[1-9]|[7-9][0-9]|[1-9][0-9][0-9]+|[^1]m)' /etc/ssh/sshd_config || result_LoginGraceTime_sshd_config=true

if [[ $result_logingracetime -eq 1 && $result_LoginGraceTime_sshd_config == true ]]; then
    echo "pass"
else
    echo "fail"
fi