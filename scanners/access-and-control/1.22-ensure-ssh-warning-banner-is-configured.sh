result=false
sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -Piq ^banner\\s+\/etc\/issue.net && result=true

if [[ $result == true ]]; then
    echo "pass"
else
    echo "fail"
fi