result=false
sshd -T -C user=root -C host="$(hostname)" -C addr="$(grep $(hostname) /etc/hosts | awk '{print $1}')" | grep -Eiq ^usepam\\s+yes && grep -Eiq '^\s*UsePAM\s+yes' /etc/ssh/sshd_config && result=true

if [[ $result == true ]]; then
    echo "pass"
else
    echo "fail"
fi