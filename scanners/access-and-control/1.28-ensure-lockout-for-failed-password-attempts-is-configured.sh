result=false

grep -Eiq "^\s*auth\s+required\s+.*\s+deny=[1-5]\s*.*$" /etc/pam.d/password-auth && grep -Eiq "^\s*auth\s+required\s+.*\s+unlock_time=(9[0-9][0-9]|[1-9][0-9][0-9][0-9])\s*.*$" /etc/pam.d/password-auth && grep -Eiq "^\s*auth\s+required\s+.*\s+deny=[1-5]\s*.*$" /etc/pam.d/system-auth && grep -Eiq "^\s*auth\s+required\s+.*\s+unlock_time=(9[0-9][0-9]|[1-9][0-9][0-9][0-9])\s*.*$" /etc/pam.d/system-auth && result=true

if [[ $result == true ]]; then
    echo "pass"
else
    echo "fail"
fi