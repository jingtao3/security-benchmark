result=false

grep -Piq "^\h*password\h+(requisite|sufficient)\h+(pam_pwhistory\.so|pam_unix\.so)\h+([^#\n\r]+\h+)?remember=([5-9]|[1-9][0-9]+)\h*(\h+.*)?$" /etc/pam.d/system-auth && result=true

if [[ $result == true ]]; then
    echo "pass"
else
    echo "fail"
fi