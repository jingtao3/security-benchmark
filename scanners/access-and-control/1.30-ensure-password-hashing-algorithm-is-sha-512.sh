result=false

grep -Eiq "^\s*password\s+sufficient\s+pam_unix.so\s+.*sha512\s*.*$" /etc/pam.d/password-auth && grep -Eiq "^\s*password\s+sufficient\s+pam_unix.so\s+.*sha512\s*.*$" /etc/pam.d/system-auth && result=true

if [[ $result == true ]]; then
    echo "pass"
else
    echo "fail"
fi