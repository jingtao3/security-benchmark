result=false

grep -Eiq "^\s*PASS_MAX_DAYS\s+(36[0-5]|3[0-5][0-9]|[1-2][0-9][0-9]|[1-9][0-9]?)\s*(\s+#.*)?$" /etc/login.defs && grep -E '^[^:]+:[^!*]' /etc/shadow | cut -d: -f5 | grep -Eq "^(36[0-5]|3[0-5][0-9]|[1-2][0-9][0-9]|[1-9][0-9]?)\s*(\s+#.*)?$" && result=true

if [[ $result == true ]]; then
    echo "pass"
else
    echo "fail"
fi