result=false

grep -Eiq "^\s*auth\s+required\s+pam_wheel.so(\s+\S+)*\s+use_uid(\s+\S+)*\s*(\s+#.*)?$" /etc/pam.d/su && grep -Eiq "^wheel:x:10:$" /etc/group && result=true

if [[ $result == true ]]; then
    echo "pass"
else
    echo "fail"
fi