val_Pass_Max_Days=99999
result_Pass_Max_Days_User=true
val_Pass_Max_Days=`grep -Ei "^\s*PASS_MAX_DAYS\s" /etc/login.defs | cut -f2`
result_Pass_Max_Days=`echo "$val_Pass_Max_Days >=30 && $val_Pass_Max_Days <= 90" | bc`

result_Pass_Max_Days_User=true
for i in `grep -E '^[^:]+:[^!*]' /etc/shadow | cut -d: -f5`; do
    if [[ i -lt 30 ]] || [[ i -gt 90 ]] ; then
        result_Pass_Max_Days_User=false
    fi
done

if [[ $result_Pass_Max_Days -eq 1 && $result_Pass_Max_Days_User == true ]]; then
    echo 'pass'
else
    echo 'fail'
fi