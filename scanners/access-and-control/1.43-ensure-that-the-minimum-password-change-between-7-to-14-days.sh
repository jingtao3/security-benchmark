val_Pass_Min_Days=0
result_Pass_Min_Days_User=true
val_Pass_Min_Days=`grep -Ei "^\s*PASS_Min_DAYS\s" /etc/login.defs | cut -f2`
result_Pass_Min_Days=`echo "$val_Pass_Min_Days >=7 && $val_Pass_Min_Days <= 14" | bc`

result_Pass_Min_Days_User=true
for i in `grep -E ^[^:]+:[^\!*] /etc/shadow | cut -d: -f4`; do
    if [[ i -lt 7 ]] || [[ i -gt 14 ]] ; then
        result_Pass_Min_Days_User=false
    fi
done

if [[ $result_Pass_Min_Days -eq 1 && $result_Pass_Min_Days_User == true ]]; then
    echo 'pass'
else
    echo 'fail'
fi