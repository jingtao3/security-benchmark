val_remember=99999
val_remember=`grep -Pi "^\h*password\h+(requisite|sufficient)\h+(pam_pwhistory\.so|pam_unix\.so)\h+([^#\n\r]+\h+)?remember=([5-9]|[1-9][0-9]+)\h*(\h+.*)?$" /etc/pam.d/system-auth | cut -d '=' -f2`
[ ! -z $val_remember ] && result_remember=`echo "$val_remember >=5 && $val_remember <= 25" | bc`

if [[ $result_remember -eq 1 ]]; then
    echo 'pass'
else
    echo 'fail'
fi