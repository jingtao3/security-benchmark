result_File_Password=`grep -E '^\s*auth\s+required\s+pam_faillock.so\s+(preauth|authfail)\s+' /etc/pam.d/password-auth | grep -Eio "deny=[0-9]+\sunlock_time=[0-9]+"`
result_File_System=`grep -E '^\s*auth\s+required\s+pam_faillock.so\s+(preauth|authfail)\s+' /etc/pam.d/system-auth | grep -Eio "deny=[0-9]+\sunlock_time=[0-9]+"`
result_Deny=true
result_Unlock_Time=true

for i in `grep -E '^\s*auth\s+required\s+pam_faillock.so\s+(preauth|authfail)\s+' /etc/pam.d/password-auth /etc/pam.d/system-auth | grep -Eio "deny=[0-9]+" | cut -d"=" -f 2`; do
    if [[ i -lt 3 ]] || [[ i -gt 8 ]] ; then
        result_Deny=false
    fi
done

for i in `grep -E '^\s*auth\s+required\s+pam_faillock.so\s+(preauth|authfail)\s+' /etc/pam.d/password-auth /etc/pam.d/system-auth | grep -Eio "unlock_time=[0-9]+" | cut -d"=" -f 2`; do
    if [[ i -lt 600 ]] || [[ i -gt 1800 ]] ; then
        result_Unlock_Time=false
    fi
done

if [[ -n $result_File_Password && -n $result_File_System && $result_Deny == true && $result_Unlock_Time == true ]]; then
    echo 'pass'
else
    echo 'fail'
fi