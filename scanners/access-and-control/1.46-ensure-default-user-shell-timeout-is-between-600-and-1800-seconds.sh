result=false
val_TMOUT=99999
val_TMOUT=`grep -Pio "TMOUT=[0-9]+" /etc/profile | cut -d"=" -f 2`
val_TMOUT_Count=`grep -Pio "TMOUT=[0-9]+" /etc/profile | wc -l`

[[ $val_TMOUT_Count -eq 1 ]] && [[ $val_TMOUT -ge 600 ]] && [[ $val_TMOUT -le 1800 ]] && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi