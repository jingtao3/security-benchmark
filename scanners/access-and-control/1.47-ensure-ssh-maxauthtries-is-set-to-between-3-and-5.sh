result=true

grep -Eiq '^\s*maxauthtries\s+([6-9]|[1-9][0-9]+)' /etc/ssh/sshd_config && result=false
[ "$result" = true ] && grep -Eiq '^\s*maxauthtries\s+([0-2])' /etc/ssh/sshd_config && result=false

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi