result=false

passwd -S shutdown | grep -Eq "shutdown\s+LK" && passwd -S halt | grep -Eq "halt\s+LK" && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi