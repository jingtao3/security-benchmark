result=false

modprobe -n -v udf | grep -q "^install" && test -z "$(lsmod | grep -e udf)" && grep -E -q "^blacklist[[:blank:]]*udf" /etc/modprobe.d/* && result=true

if [ "$result" == true ]; then
	echo "pass"
else
	echo "fail"
fi
