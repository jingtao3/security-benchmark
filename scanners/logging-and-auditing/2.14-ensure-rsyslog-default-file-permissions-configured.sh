#!/usr/bin/env bash
result=0

for p in `find /etc/rsyslog.conf ; find /etc/rsyslog.d/ -name *.conf` ; do
    [[ -f $p ]] && file_path=$file_path" ${p}"
done

if [[ -n $file_path ]] && [[ -n $(sed -rn 's/^\$FileCreateMode\s*//gp' $file_path) ]]; then
    for num in $(sed -rn 's/^\$FileCreateMode\s*//gp' $file_path); do
        result=0
        echo $num | grep -Pq '[0][0-6][0-4][0]' && result=1
        [[ $result == 0 ]] && echo 'fail' && break
    done
else
    echo 'fail'
fi

[[ $result == 1 ]] && echo 'pass'
