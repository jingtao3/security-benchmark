result_rsyslog=false
result_rsyslog_d=false
conf_file_path=`ls /etc/rsyslog.d/*.conf`

if [[ -a /etc/rsyslog.conf && -a $conf_file_path ]]; then
    grep -q "^*.*[^I][^I]*@" /etc/rsyslog.conf && result_rsyslog=true
    grep -q "^*.*[^I][^I]*@" /etc/rsyslog.d/*.conf && result_rsyslog_d=true
elif [[ -a /etc/rsyslog.conf ]]; then
    grep -q "^*.*[^I][^I]*@" /etc/rsyslog.conf && result_rsyslog=true
elif [[ -a $conf_file_path ]]; then
    grep -q "^*.*[^I][^I]*@" /etc/rsyslog.d/*.conf && result_rsyslog_d=true
fi

if [[ $result_rsyslog == true || $result_rsyslog_d == true ]]; then
    echo "pass"
else
    echo "fail"
fi