result=false

if [[ -a /etc/systemd/journald.conf ]]; then
    grep -qe ^\s*ForwardToSyslog /etc/systemd/journald.conf && result=true
    if [[ $result == true ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "fail"
fi