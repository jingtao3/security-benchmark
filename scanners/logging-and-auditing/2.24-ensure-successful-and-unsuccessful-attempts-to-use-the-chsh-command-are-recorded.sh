#!/usr/bin/env bash
result=false

auditctl -l | grep -Pq "^\-a\s+always,exit\s+-S\s+all\s+-F\s+path=/usr/bin/chsh\s+-F\s+perm=x\s+-F\s+auid>=1000\s+-F\s+auid!=-1 -F\s+key=.*$" && grep -Pq "^\-a\s+always,exit\s+\-F\s+path=/usr/bin/chsh\s+\-F\s+perm=x\s+\-F\s+auid>=1000\s+\-F\s+auid!=4294967295\s+\-k\s.*$" /etc/audit/rules.d/stig.rules && result=true

if [ "$result" == true ]; then
    echo "pass"
else
    echo "fail"
fi