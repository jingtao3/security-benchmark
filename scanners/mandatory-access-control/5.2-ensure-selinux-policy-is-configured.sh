#!/bin/bash
SELINUX=`grep -E "^\s*SELINUX=disabled\b" /etc/selinux/config` 
SELINUX_R=`echo $?` # include 0

if [ $SELINUX_R != 0 ];then
	SELINUXTYPE=`grep -E "^\s*SELINUXTYPE=mls\b" /etc/selinux/config` 
	SELINUXTYPE_R=`echo $?` # include 0
else
	SELINUXTYPE_R=1
fi

if [[ `sestatus | grep Loaded` =~ "mls" ]] && [ $SELINUXTYPE_R == 0 ];then
	echo "pass";
else
	echo "fail";
fi
