#!/bin/bash
TYPE=`grep -Ei "^\s*SELINUX=enforcing" /etc/selinux/config` 
TYPE_R=`echo $?` # include 0

if [ `getenforce` == "Enforcing" ] && [ $TYPE_R == 0 ];then
        echo "pass";
else
        echo "fail";
fi

