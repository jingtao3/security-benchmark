if [ "$(rpm -qa telnet)" ]; then
    result=$(systemctl is-enabled telnet.socket)
    if [[ $result != enabled ]]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi
