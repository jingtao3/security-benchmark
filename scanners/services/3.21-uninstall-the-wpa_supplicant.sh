result=false

rpm -q wpa_supplicant | grep -Psiq "^package\s+wpa_supplicant\s+is\s+not\s+installed$" && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi