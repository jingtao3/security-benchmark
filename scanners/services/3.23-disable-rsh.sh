if [ "$(rpm -qa rsh)" ]; then
    result=$(systemctl is-enabled rsh.socket)
    if [ $result != enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi