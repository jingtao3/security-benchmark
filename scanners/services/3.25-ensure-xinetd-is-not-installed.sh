#!/usr/bin/env bash
result=false

rpm -q xinetd | grep -Psiq "^package\s+xinetd\s+is\s+not\s+installed$" && result=true

if [ "$result" == true ]; then
	echo "pass"
else
	echo "fail"
fi