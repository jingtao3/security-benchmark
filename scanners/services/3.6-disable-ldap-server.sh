if [ "$(rpm -qa openldap-servers)" ]; then
    result=$(systemctl is-enabled slapd)
    if [ $result != enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi