if [ "$(rpm -qa dhcp-server)" ]; then
    result=$(systemctl is-enabled dhcpd)
    if [ $result != enabled ]; then
        echo "pass"
    else
        echo "fail"
    fi
else
    echo "pass"
fi