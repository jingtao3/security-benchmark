#!/usr/bin/env bash
[[ -f /boot/grub2/grub.cfg ]] && file_path='/boot/grub2/grub.cfg'
[[ -f /boot/grub2/grubenv ]] && file_path=$file_path' /boot/grub2/grubenv'
[[ -f /boot/grub2/user.cfg ]] && file_path=$file_path' /boot/grub2/user.cfg'
result=0

if [[ -n $file_path ]] ; then
    for access in $file_path; do
        result=0
        stat -c '%U:%G' $access | grep -Pq "^root\:root$" && stat -c '%a' $access | grep -Pq "^[0-7]00$" && result=1
        [[ $result == 0 ]] && echo 'fail' && break
    done
else
    echo 'fail'
fi

[[ $result == 1 ]] && echo 'pass'
