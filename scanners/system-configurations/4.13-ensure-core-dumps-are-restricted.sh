result=false

grep -Pq "^\s*\*\s+hard\s+core\s+0\s*(\s+#.*)?$" /etc/security/limits.conf && grep -Pq "^\s*fs\.suid_dumpable\s*=\s*0\s*(\s+#.*)?$" /etc/sysctl.conf /etc/sysctl.d/* && sysctl fs.suid_dumpable|grep -Pq "fs\.suid\_dumpable\s+=\s+0" && result=true

if [ "$result" = true ] ; then
    echo "pass"
else
    echo "fail"
fi