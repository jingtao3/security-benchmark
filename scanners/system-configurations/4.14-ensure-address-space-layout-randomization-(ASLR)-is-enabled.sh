result=false

sysctl kernel.randomize_va_space|grep -Psq "^kernel\.randomize\_va\_space\s+=\s+2$" && grep -Psq "^kernel\.randomize_va_space\s+=\s+2" /run/sysctl.d/*.conf /etc/sysctl.d/*.conf /usr/local/lib/sysctl.d/*.conf /usr/lib/sysctl.d/*.conf /lib/sysctl.d/*.conf /etc/sysctl.conf && result=true

if [ "$result" = true ] ; then
    echo "pass"
else
    echo "fail"
fi