result=false

stat -c "%a-%U-%G" /etc/gshadow | grep -Pq '^[0]\-root\-root$' && result=true

if [ "$result" = true ] ; then
    echo "pass"
else
    echo "fail"
fi
