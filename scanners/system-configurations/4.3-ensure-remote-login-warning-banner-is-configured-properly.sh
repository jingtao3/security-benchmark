result=false

grep -Eiq "(\\\v|\\\r|\\\m|\\\s|$(grep '^ID=' /etc/os-release | cut -d= -f2 | sed -e 's/"//g'))" /etc/issue.net || result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi