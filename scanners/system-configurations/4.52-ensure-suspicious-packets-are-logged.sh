result=false

sysctl net.ipv4.conf.all.log_martians | grep -Psq "^net\.ipv4\.conf\.all\.log_martians\s+=\s+1$" && sysctl net.ipv4.conf.default.log_martians | grep -Psq "^net\.ipv4\.conf\.default\.log_martians\s+=\s+1$" && grep -q "net\.ipv4\.conf\.all\.log_martians" /etc/sysctl.conf /etc/sysctl.d/* && grep -q "net\.ipv4\.conf\.default\.log_martians" /etc/sysctl.conf /etc/sysctl.d/* && result=true

if [ "$result" = true ]; then
    echo "pass"
else
    echo "fail"
fi