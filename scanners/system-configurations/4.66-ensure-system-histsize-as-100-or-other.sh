#!/bin/bash
source /etc/profile
HIST=`echo $HISTSIZE`
HIST_FILE=`grep -iP "^HISTSIZE" /etc/profile`

if [[ $HIST == "100" ]] && [[ $HIST_FILE == "HISTSIZE=100" ]];then
    echo "pass"
else
    echo "fail"
fi
