#!/bin/bash

for i in `stat -c "%a-%U-%G" {/etc/ssh/*key,/etc/ssh/*key.pub}`
do
    if ! [[ $i =~ "400" ]];then
        echo "fail"
        exit 1
    fi
done
echo "pass"