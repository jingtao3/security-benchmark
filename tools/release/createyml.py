#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import yaml
import time
import os
import shutil
from pprint import pprint
from collections import OrderedDict
from pathlib import Path
from input_parm import run_input_parm, loggerr

loger = loggerr("createyml")
rel = run_input_parm()
args = rel[0]
curdir = rel[1]
abscurdir = rel[2]
argpath = rel[3]
absargpath = rel[4]
ymlpath = rel[5]

id_status = []

def create_yaml():
    benchmark = list(absargpath.iterdir())
    benchmark.sort()
    for item in benchmark:
        if item.is_file():
            continue
        for root, dirs, files in os.walk(item):
            if not dirs:
                loger.info("dirs为空，即{}下没有子目录，开始处理{}下的文件".format(root, root))
                sort_list = sorted(files, key=lambda mf: int(mf.split('-', 1)[0].split('.')[1]))
                yml_forfiles(root, sort_list)
            else:
                loger.error("{} 下有子目录：{},代码不适用".format(root, dirs))
                loger.error("type error: {} 目录下只能是md文件，不能有子目录: {}".format(root, dirs))
                raise Exception("type error: {} 目录下只能是md文件，不能有子目录: {}".format(root, dirs))
    loger.info("total count md: {}".format(len(id_status)))
    benchmark_config_rule_dict = {}
    update_benchmark_config_rule_dict(benchmark_config_rule_dict)
    write_yml(benchmark_config_rule_dict)


def yml_forfiles(root, sort_list):
    absymlpath = Path(root)
    for file in sort_list:
        absfilemdpath = absymlpath.joinpath(file)
        if file.startswith("new"):
            continue
        if file.endswith(".pdf"):
            continue
        if not file.endswith(".md"):
            continue
        get_id_status(file)


def get_id_status(file):
    idd = file.rsplit('.', 1)[0]
    status = "enabled"
    id_status.append([idd, status])


def update_benchmark_config_rule_dict(benchmark_config_rule_dict):
    benchmark_config_rule_dict.update({"benchmarks": []})
    for item in id_status:
        benchmark_config_rule_dict["benchmarks"].append({"id": item[0], "status": item[1]})


def write_yml(benchmark_config_rule_dict):
    loger.info("ymlpath >>> {}".format(ymlpath))
    if ymlpath.exists() and ymlpath.is_file():
        ymlpath.unlink()
    with open(ymlpath, 'w', encoding='utf-8') as f:
        # yaml.safe_dump(data=benchmark_config_rule_dict, stream=f, sort_keys=False, default_flow_style=False)
        yaml.dump(data=benchmark_config_rule_dict, stream=f, sort_keys=False, default_flow_style=False)


if __name__ == "__main__":
    args_list = sys.argv
    if Path(sys.argv[0]).name == "createyml.py":
        if "-o" in args_list:
            raise Exception("参数错误，生成yml时不需要加-o参数")
    create_yaml()
